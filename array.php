<?php

     // $prise_basic - Базовая стоимость в гривне  $v['price_val']
    // discount_type  'percent' и  'value' - Тип скидки
    // discount_val - Значение скидки 15% и 1200UAH

    function getPriceWithDiscount($prise_basic, $discount_type, $discount_val) {
 
           switch(true){
            case $discount_type === 'percent': return round( $prise_basic - ($prise_basic /100 * $discount_val)) ;
             break;
            case $discount_type === 'value': return round($prise_basic- $discount_val);
             break;
         
          }
           
          }
   
    // $result_finish - getPriceWithDiscount($v['price_val'], $v['discount_type'], $v['discount_val'])
    // $currency_usd - 27.1
    // $currency_eur - 30.2

    function convertPrice( $result_finish, $currency_usd, $currency_eur){
     
        switch(true){
            case $_SESSION['select'] === 'usd': return round( $result_finish / $currency_usd);
            break;
            case $_SESSION['select'] === 'eur': return round( $result_finish / $currency_eur) ;
            break;
                  
        }
       }

        $currency = [  
            'uah' => [
                'name' => 'Гривна',
                'course' => 1,
            ],
            'usd' => [
                'name' => 'Доллар',
                'course' => 27.1,
            ],                                                                                                
            'eur' => [
                'name' => 'Евро',
                'course' => 30.2
            ]
        ];
        
                
        $cameras = [
            [
                'title' => 'Цифровая фотокамера Canon EOS 4000D 18-55',
                'price_val' => 11700,
                'discount_type' =>'percent', 
                'discount_val' => 15 
              
            ],
            [
                'title' => 'Цифровая фотокамера Nikon D500 + AF-S DX 16-80VR',
                'price_val' => 75800,
                'discount_type' => 'percent',
                'discount_val' => 15 
            ],
            [
                'title' => 'Цифровая фотокамера Canon EOS 90D Body',
                'price_val' => 36800,
                'discount_type' => 'percent',
                'discount_val' => 15 
            ],
            [
                'title' => 'Цифровая фотокамера Canon EOS 80D Body',
                'price_val' => 32700,
                'discount_type' => 'value', 
                'discount_val' => 1200 
            ],
            [
                'title' => 'Цифровая фотокамера Nikon D850 Body',
                'price_val' => 82700,
                'discount_type' => 'percent',
                'discount_val' => 15
            ],
            [
                'title' => 'Цифровая фотокамера Fujifilm X-T30 Body Charcoal Silver',
                'price_val' => 28800,
                'discount_type' => 'value',
                'discount_val' => 1200
            ],
            [
                'title' => 'Цифровая фотокамера Sony Alpha A7S III',
                'price_val' => 112200,
                'discount_type' => 'percent',
                'discount_val' => 15 
            ],
            [
                'title' => 'Цифровая фотокамера Nikon Z7 24-70 f4 Kit + FTZ Adapter + 64Gb XQD Kit',
                'price_val' => 108400,
                'discount_type' =>  'percent',
                'discount_val' => 15 
            ],
            [
                'title' => 'Цифровая фотокамера Fujifilm X-T30 Body Black',
                'price_val' => 22800,
                'discount_type' => 'value',
                'discount_val' => 1200 
            ],
            [
                'title' => 'Цифровая фотокамера Nikon Z50 + 16-50 VR + FTZ Adapter Kit',
                'price_val' => 30200,
                'discount_type' =>  'percent',
                'discount_val' => 15
            ]
            ];


?>