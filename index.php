<?php
session_start();
if ((empty($_SESSION['select']))){
    $_SESSION['select'] = 'uah';
}


require_once 'array.php';

?>


<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info</title>
    <meta name="description" content="Info">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header id="header" class="containar-fluid">
        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="main_menu_brend">
                    <a class="navbar-brand" href="index.php">Info</a>
                </div>

                <button class="navbar-toggler" type="button" name = "button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


            </nav>
        </div>

    </header>
    
    <!-- ____________________________________select________________________________________ -->

    <section id="main_page">
        <div class="container">
            <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 contacts_form">

                    <h1>Выберите валюту</h1>

                    <form action="add_session.php" method="post">

                        <div class="mb-3">
                            <select class="form-select" name="select_id" aria-label="Default select example">

                                <option name="uah" <?php if ($_SESSION['select'] === 'uah') : ?> selected <?php endif; ?>value="uah">Гривна</option>
                                <option <?php if ($_SESSION['select'] === 'usd') : ?> selected <?php endif; ?>value="usd">Доллар</option>
                                <option <?php if ($_SESSION['select'] === 'eur') : ?> selected <?php endif; ?>value="eur">Евро</option>


                            </select>
                        </div>

                        <div class="mb-3">
                            <button type="submit" class="btn btn-outline-secondary">Конвертировать</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </section>
    <!-- _____________________tables_main__________________________ -->

    <section id="tables_main">
        <div class="containar-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12  d-flex justify-content-center">
                        <table class="table table-hover table-warning table-striped">
                            <thead class="table-dark">
                                <tr>
                                    <th scope="col">Название товара</th>
                                    <th scope="col">Цена товара в UAH/USD/EUR</th>
                                    <th scope="col">Тип скидки</th>
                                    <th scope="col">Цена товара со скидкой UAH/USD/EUR</th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($cameras as $cameras_key => $v) : ?>

                                    <tr class="table-warning table_color">
                                        <th scope="row"><?= $v['title']; ?></th>

                                        <?php if ($_SESSION['select'] === 'usd') : ?>
                                            <th scope="row"><?= round($currency['uah']['course'] * $v['price_val'] / $currency['usd']['course']) . ' USD'; ?></th>

                                        <?php elseif ($_SESSION['select'] === 'eur') : ?>
                                            <th scope="row"><?= round($currency['uah']['course'] * $v['price_val'] / $currency['eur']['course']) . ' EUR'; ?></th>

                                        <?php elseif ($_SESSION['select'] === 'uah') : ?>
                                            <th scope="row"><?= $v['price_val'] . ' UAH'; ?></th>
                                                                                                                              
                                        <?php endif; ?>



                                        <?php if ($_SESSION['select'] === 'uah') : ?>
                                            <th scope="row"><?= $v['discount_type']; ?></th><th scope="row"><?= getPriceWithDiscount($v['price_val'], $v['discount_type'], $v['discount_val']) . ' UAH'; ?>
                                           

                                        <?php elseif ($_SESSION['select'] === 'usd') : ?>
                                            <th scope="row"><?= $v['discount_type']; ?></th><th scope="row"><?= convertPrice(getPriceWithDiscount($v['price_val'], $v['discount_type'], $v['discount_val']), 27.1, 30.2 ) . ' USD'; ?></th>

                                            <?php elseif ($_SESSION['select'] === 'eur') : ?>
                                                <th scope="row"><?= $v['discount_type']; ?></th><th scope="row"><?= convertPrice(getPriceWithDiscount($v['price_val'], $v['discount_type'], $v['discount_val']), 27.1, 30.2 ) . ' EUR'; ?></th>

                                        <?php endif; ?>

                                    </tr>

                                <?php endforeach; ?>

                            </tbody>
                            <thead class="table-dark">
                                <tr>
                                <th scope="col">Название товара</th>
                                    <th scope="col">Цена товара в UAH/USD/EUR</th>
                                    <th scope="col">Тип скидки</th>
                                    <th scope="col">Цена товара со скидкой UAH/USD/EUR</th>
                                </tr>
                            </thead>
                        </table>

                    </div>


                </div>
            </div>
        </div>
    </section>
    <!-- ________________footer_______________________________ -->

    <footer id="footer" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col footer_name d-flex flex-row">
                    Info
                </div>
                <div class="col footer_copyright d-flex flex-row-reverse">
                    Copyright © 2021
                </div>
            </div>
        </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>

</html>